import argparse
import datetime
import math
import os
from PIL import Image
import pygame
import random
import sys


# TODO: Delta-timing + simulationspeed

# ------------------------------- Global Vars ------------------------------- #

CAPTION = "Mega Advanced Particle Simulator (MAPS)"
SCREEN_SIZE = (400, 600)
BACKGROUND_COLOUR = (0, 0, 0)
STANDARD_AMOUNT = 40
RADIUS_RANGE = (10, 20)  # Keep the radius small to avoid clipping
SPEED_RANGE = (-10, 10)
TEXT_SIZE = 15
FPS = 60


# ------------------------------ Global Helper ------------------------------ #

def dot(x, y):
    """ Calculates scalar/dot-product """
    s = 0
    for i in range(len(x)):
        s += x[i] * y[i]
    return s


# --------------------------------- Particles ------------------------------- #

class Particle:
    """A single particle"""
    def __init__(self, velX, velY, posX, posY, radius, colour):
        self.velX = velX
        self.velY = velY
        self.posX = posX
        self.posY = posY
        self.radius = radius
        self.colour = colour  # RGB-tuple

# -------------------------------- P-Movement ------------------------------- #

    def move(self):

        # Check for x-axis collisions:
        if (self.posX + self.radius + self.velX >
                SCREEN_SIZE[0]):
            self.posX += 2 * (SCREEN_SIZE[0] - self.radius - self.posX) - \
                self.velX
            self.velX *= -1
        elif (self.posX + self.velX < self.radius):
            self.posX += 2 * (self.radius - self.posX) - self.velX
            self.velX *= -1
        else:
            self.posX += self.velX

        # Check for y-axis collisions:
        if (self.posY + self.radius + self.velY >
                SCREEN_SIZE[1]):
            self.posY += 2 * (SCREEN_SIZE[1] - self.radius - self.posY) - \
                self.velY
            self.velY *= -1
        elif (self.posY + self.velY < self.radius):
            self.posY += 2 * (self.radius - self.posY) - self.velY
            self.velY *= -1
        else:
            self.posY += self.velY

    def particle_collision(particle1, particle2):
        # TODO: Performance
        n = (particle2.posX - particle1.posX,
             particle2.posY - particle1.posY)
        distance_to_center = math.hypot(n[0], n[1])

        # Do they collide?
        if distance_to_center < particle2.radius + particle1.radius:

            if abs(distance_to_center) >= 0.0000000000000001:
                normN = (n[0] / distance_to_center, n[1] / distance_to_center)
                n = normN
            else:
                # Default normal
                n = (1, 0)

            if ((dot(n, (particle1.velX, particle1.velY)) > 0 and
                    dot(n, (particle2.velX, particle2.velY)) > 0) or
                    (dot(n, (particle1.velX, particle1.velY)) < 0 and
                     dot(n, (particle2.velX, particle2.velY)) < 0)):

                # new_part1_velX = particle2.velX
                # new_part1_velY = particle2.velY
                # new_part2_velX = particle1.velX
                # new_part2_velY = particle1.velY

                # particle1.velX = new_part1_velX
                # particle1.velY = new_part1_velY
                # particle2.velX = new_part2_velX
                # particle2.velY = new_part2_velY

                (particle1.velX, particle2.velX) = \
                    (particle2.velX, particle1.velX)
                (particle1.velY, particle2.velY) = \
                    (particle2.velY, particle1.velY)
            else:
                # Funny collision-math:
                new_part1_velX = particle1.velX - \
                    2 * dot(n, (particle1.velX, particle1.velY)) * n[0]
                new_part1_velY = particle1.velY - \
                    2 * dot(n, (particle1.velX, particle1.velY)) * n[1]

                part1_vel = math.hypot(new_part1_velX, new_part1_velY)

                new_part2_velX = particle2.velX - \
                    2 * dot(n, (particle2.velX, particle2.velY)) * n[0]
                new_part2_velY = particle2.velY - \
                    2 * dot(n, (particle2.velX, particle2.velY)) * n[1]

                part2_vel = math.hypot(new_part2_velX, new_part2_velY)

                if abs(part1_vel) >= 0.0000000000000001:
                    particle1.velX = (new_part1_velX / part1_vel) * part2_vel
                    particle1.velY = (new_part1_velY / part1_vel) * part2_vel
                else:
                    particle1.velX = n[0] * -part2_vel
                    particle1.velY = n[1] * -part2_vel

                if abs(part2_vel) >= 0.0000000000000001:
                    particle2.velX = (new_part2_velX / part2_vel) * part1_vel
                    particle2.velY = (new_part2_velY / part2_vel) * part1_vel
                else:
                    particle2.velX = n[0] * part1_vel
                    particle2.velY = n[1] * part1_vel

            # Move them out from one another to avoid clipping:
            shift = math.ceil(((particle2.radius + particle1.radius -
                              distance_to_center) + 1) / 2)

            #  print("Collision with shift = {:6.3f}".format(shift))

            particle1.posX = particle1.posX - n[0] * shift
            particle1.posY = particle1.posY - n[1] * shift
            particle2.posX = particle2.posX + n[0] * shift
            particle2.posY = particle2.posY + n[1] * shift

# --------------------------------- P-Helper -------------------------------- #

    def change_colour(self):
        self.colour = (random.randint(50, 255),
                       random.randint(50, 255),
                       random.randint(50, 255))


# ----------------------------------- App ----------------------------------- #

class App:
    """ Class responsible for program control flow. """
    def __init__(self):
        self.DISPLAYSURF = pygame.display.get_surface()
        self.font = pygame.font.SysFont('mono', TEXT_SIZE)
        self.clock = pygame.time.Clock()
        self.running = True
        self.disco = False
        self.overlay = True
        self.overlay2 = False
        self.paused = False
        self.playtime = 0.0
        self.keys = pygame.key.get_pressed()
        self.particles = self.create_particles()

# ---------------------------------- Frame ---------------------------------- #

    def draw_single_frame(self):
        """ Calculate new position for given particles """

        # Move and check collisions:
        for i, particle in enumerate(self.particles):

            if self.disco:
                particle.change_colour()

            # Movement:
            particle.move()
            # Collision-check with other particles:
            for otherparticle in self.particles[i + 1:]:
                Particle.particle_collision(particle, otherparticle)

            # If one clips out:
            if particle.posX > SCREEN_SIZE[0] or \
               particle.posY > SCREEN_SIZE[1] or \
               particle.posX < 0 or \
               particle.posY < 0:
                # print("A particle escaped! Call the police!")
                # delete him:
                self.particles.remove(particle)

            # Draw each particle:
            pygame.draw.circle(self.DISPLAYSURF, particle.colour,
                               (int(round(particle.posX)),
                                int(round(particle.posY))),
                               particle.radius, 0)

    def draw_paused_frame(self):

        # Draw particles
        for particle in self.particles:
            pygame.draw.circle(self.DISPLAYSURF, particle.colour,
                               (int(round(particle.posX)),
                                int(round(particle.posY))),
                               particle.radius, 0)

        # Draw pause text
        if self.overlay:
            x = SCREEN_SIZE[0] - 65
            y = SCREEN_SIZE[1] - TEXT_SIZE + 1
            self.DISPLAYSURF.blit(self.font.render("Paused", True,
                                                   (255, 255, 255)), (x, y))


# --------------------------- Particle Functions ---------------------------- #

    def create_particles(self):
        """ Creates a list of particles. """

        self.particles = []

        for i in range(self.get_amount()):
            self.particles.append(Particle(random.randint(SPEED_RANGE[0],
                                                          SPEED_RANGE[1]),
                                           random.randint(SPEED_RANGE[0],
                                                          SPEED_RANGE[1]),
                                           random.randint(0, SCREEN_SIZE[0]),
                                           random.randint(0, SCREEN_SIZE[1]),
                                           random.randint(RADIUS_RANGE[0],
                                           RADIUS_RANGE[1]),
                                           (random.randint(50, 255),
                                           random.randint(50, 255),
                                           random.randint(50, 255))))

            pygame.draw.circle(self.DISPLAYSURF,
                               self.particles[i].colour,
                               (self.particles[i].posX,
                                self.particles[i].posY),
                               self.particles[i].radius, 0)

        return self.particles

    def create_single_particle(self, mouseposition):

        velX = random.randint(SPEED_RANGE[0], SPEED_RANGE[1])
        velY = random.randint(SPEED_RANGE[0], SPEED_RANGE[1])
        posX, posY = mouseposition
        radius = random.randint(RADIUS_RANGE[0], RADIUS_RANGE[1])
        colour = (random.randint(50, 255),
                  random.randint(50, 255),
                  random.randint(50, 255))

        self.particles.append(Particle(velX, velY, posX, posY, radius, colour))

        pygame.draw.circle(self.DISPLAYSURF, colour, (posX, posY), radius, 0)

    def delete_single_particle(self, mouseposition):
        particle = self.find_particle(mouseposition)
        if particle is not None:
            self.particles.remove(particle)

    def find_particle(self, mouseposition):
        for particle in self.particles:
            if math.hypot(particle.posX - mouseposition[0], particle.posY -
                          mouseposition[1]) <= particle.radius:
                return particle

    def change_part_colour(self):  # Just for fun
        """ Changes particle colour to random RGB-colour """
        for particle in self.particles:
            particle.change_colour()

    def unstuck_particle(self):

        for particle in self.particles:
            if particle.velX == 0 and particle.velY == 0:
                particle.velX = 1
                particle.velY = 1

# --------------------------------- Helper ---------------------------------- #

    def get_amount(self):
        """ Parse the CLI for amount of particles """
        global STANDARD_AMOUNT

        parser = argparse.ArgumentParser(description='Mega Advanced Particle \
                                         Simulator (MAPS)')
        parser.add_argument('--number', '-n', type=int, help='specifies \
                            the number of particles')
        args = parser.parse_args()

        if args.number is not None:
            return args.number
        else:
            return STANDARD_AMOUNT

    def take_screenshot(self):

        timestamp = ("{:%d-%m-%Y_%H:%M:%S}".format(datetime.datetime.now()))
        screenshot = ("{}/screenshot_{}.png").format(os.getcwd(), timestamp)

        try:
            myimage = Image.new("RGB", SCREEN_SIZE, BACKGROUND_COLOUR)
            pixels = myimage.load()
        except:
            print("Error loading image!")

        # There might be a less performance-tanking method...
        pixelarray = pygame.PixelArray(self.DISPLAYSURF)
        for i in range(SCREEN_SIZE[0]):
            for j in range(SCREEN_SIZE[1]):
                pixels[i, j] = pixelarray[i][j]

        myimage.save(screenshot)


# ---------------------------------- Keys ----------------------------------- #

    def event_loop(self):

        global FPS

        # Get pressed keys:
        self.keys = pygame.key.get_pressed()

        # Work through keys that are hold down:
        if self.keys[pygame.K_UP]:
            if FPS <= 1000:
                FPS += 1
        if self.keys[pygame.K_DOWN]:
            if FPS >= 1:
                FPS -= 1

        if pygame.mouse.get_pressed()[0]:
            self.create_single_particle(pygame.mouse.get_pos())

        if pygame.mouse.get_pressed()[2]:
            self.delete_single_particle(pygame.mouse.get_pos())

        # Single press events:
        for event in pygame.event.get():
            if event.type == pygame.QUIT or (event.type == pygame.KEYUP and
                                             event.key == pygame.K_ESCAPE):
                self.running = False

            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                self.overlay = not self.overlay

            if event.type == pygame.KEYDOWN and event.key == pygame.K_DELETE:
                self.particles = []

            if event.type == pygame.KEYDOWN and event.key == pygame.K_F1:
                self.overlay2 = not self.overlay2

            if event.type == pygame.KEYDOWN and event.key == pygame.K_r:
                self.change_part_colour()

            if event.type == pygame.KEYDOWN and event.key == pygame.K_e:
                self.unstuck_particle()

            if event.type == pygame.KEYDOWN and event.key == pygame.K_d:
                self.disco = not self.disco

            if event.type == pygame.KEYDOWN and event.key == pygame.K_p:
                self.paused = not self.paused

            if event.type == pygame.KEYDOWN and event.key == pygame.K_F12:
                self.take_screenshot()

# -------------------------------- Main-loop -------------------------------- #

    def main_loop(self):
        """ Controls the main loop """
        while self.running:

            # Get event-loop:
            self.event_loop()

            # Get a fresh screen:
            self.DISPLAYSURF.fill(BACKGROUND_COLOUR)

            # Timekeeping:
            milliseconds = self.clock.tick(FPS)

            # Calculate frame:
            if not self.paused:
                self.draw_single_frame()
                self.playtime += milliseconds / 1000.0
            else:
                self.draw_paused_frame()

            # Add FPS-Counter:
            if self.overlay:

                text_hud1 = ("FPS: {:6.1f}{} (Internal: {}){}"
                             "Running: {:6.1f} SECONDS"
                             ).format(self.clock.get_fps(), " " * 2, FPS,
                                      " " * 5, self.playtime)

                self.DISPLAYSURF.blit(self.font.render(text_hud1, True,
                                      (255, 255, 255)), (0, 0))
            if self.overlay2:
                text_hud2 = (
                    "[ESC]      - Exit \n"
                    "[DEL]      - Delete all particles \n"
                    "[SPACE]    - Deactivate performance-overlay \n"
                    "[F1]       - Deactivate help-overlay \n"
                    "[F12]      - Take screenshot \n"
                    "[D]        - Ugly disco-mode \n"
                    "[E]        - Unstuck particles \n"
                    "[P]        - Pause \n"
                    "[R]        - Change particle color randomly \n"
                    "[UP, DOWN] - Change render and physics speed \n"
                    "[MB-LEFT]  - Add particles at pointer position \n"
                    "[MB-RIGHT] - Delete particle")

                spacing = SCREEN_SIZE[1] - 12 * (TEXT_SIZE + 1) - 5

                for line in text_hud2.splitlines():
                    self.DISPLAYSURF.blit(self.font.render(line, True,
                                          (255, 255, 255)), (3, spacing))
                    spacing += TEXT_SIZE + 1

            # Update screen:
            pygame.display.update()


# -------------------------------    ||||    -------------------------------- #
#                                                                             #
#                      -- .- .. -.   MAIN   -- .- .. -.                       #
#                                                                             #
# -------------------------------    ||||    -------------------------------- #

def main():
    """ Initialize; create an App; and start the main loop. """
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pygame.init()
    pygame.display.set_caption(CAPTION)
    pygame.display.set_mode(SCREEN_SIZE)
    App().main_loop()
    pygame.quit()
    sys.exit()


if __name__ == "__main__":
    main()
